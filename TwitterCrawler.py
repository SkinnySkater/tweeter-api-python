from tweepy import StreamListener

import Config
import tweepy

auth = tweepy.OAuthHandler(Config.consumer_key, Config.consumer_secret)
auth.set_access_token(Config.access_token, Config.access_secret)
api = tweepy.API(auth)