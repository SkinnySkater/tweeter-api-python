#Twitter API Parser & Gephi Post treatment
	This is a short programm and tutorial to show how to use Tweepy API and 
	to complete the crawled data with Gephi software and analyse the Twitter stream api
	converted into adjacent dataset in CSV format.

#Powered by : 
![alt text][logo-python]
![alt text][logo-twitter]
![alt text][logo-gephi]

[logo-python]: http://icons.iconarchive.com/icons/cornmanthe3rd/plex/64/Other-python-icon.png "Python 2.6 >"

[logo-twitter]: http://icons.iconarchive.com/icons/tinylab/android-lollipop-apps/64/Twitter-icon.png "tweepy 3.5.0"

[logo-gephi]: http://2.bp.blogspot.com/-GXxzHqs3QMY/Uxzbc1I4xkI/AAAAAAAAAvE/oGQib7GVa0U/s1600/gephi_logo.png "Gephi"

![Gephi twitter Project](https://i2.wp.com/schoolofdata.org/files/2014/08/1-YI3QuyzhfVCzKQeGLcY4Mw.png?ssl=1)

###
##Prerequisites
###
Python 2.7 > or 3
Pip & easyinstall
Windows or Linux OS or MacOs
[Gephi](https://gephi.org/users/download "Download On Official Website")

Having A tweeter Developper Account in order to get credentials for the API

###
##Installation
###
Install Tweepy library :
	[See the official doc](http://docs.tweepy.org/en/v3.5.0/install.html)
Feel the Config.py


#How To Run The programm ?
* Run The Streem parser:  python TweepyParser.py Whatever_Hashtag
* Wait for a while to have enough tweet stored
* Run the Graph generator:	 python GraphGenerator.py
* OPen Gephi:
	Open Gephi and create a new project (File -> New Project). Import your MyGraph.csv data from the Data Lab, Links Tab, and Import Spreadsheet.
* Then you can Enjoy making post treatment of you data, here is some tutorial to handle Gephi [tuto](https://gephi.org/users/quick-start)
* ENJOY!

#Why Making This Script Could Be Useful For you !?
	This kind of application is helpful to recognized the efficiency and scope of a tweet , it’s mean we can quantify the amount of people affected by a user , the user could be company account.
	An localise the  mostly efficient users to spread the informations.
	To go further we can monitor those data to get a specific range of user’s profile which are most likely to spread an ad or a companie’s tweet .

##Screen
	You will be able to find some screen about Gephi software on screen directory.
