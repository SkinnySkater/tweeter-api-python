# -*- coding: utf-8 -*-
import csv
import json
import re
import Config
import pprint
import sys

if __name__ == '__main__':
	lis1 =  []
	for line in open(Config.save_json_file):
		try:
			js = json.loads(line)
			lis1.append(js)
		except Exception as e:
			print("error")

	for i in lis1:
		emitter = i['user']['screen_name']
		text = i['text']

		target = []
		for k in re.findall(r'@\D{1,15}?\s',i['text']):
			k = k.strip()
			if (':' in k):
				k = k[:-1]
			k = k[1:]
			target.append(k)

		print("emitter:" + emitter)
		print("Targets:")

		for j in target:
			print(j.encode('utf-8'))
			with open(Config.fileGraph,'a') as fi:
				writer = csv.writer(fi)
				writer.writerow([emitter.encode('utf-8'),j.encode('utf-8')])
