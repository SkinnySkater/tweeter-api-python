import Config
import tweepy
import sys
from tweepy import StreamListener

auth = tweepy.OAuthHandler(Config.consumer_key, Config.consumer_secret)
auth.set_access_token(Config.access_token, Config.access_secret)
api = tweepy.API(auth)



class MyListener(StreamListener):

    def on_data(self,data):
        with open(Config.save_json_file,'a') as f:
            f.write(data)


    def on_error(self,status):
        print(status)
        return True


if __name__ == "__main__":
	myStreamListener = MyListener()
	myStream = tweepy.Stream(auth = api.auth, listener=MyListener())
	myStream.filter(track=[str(sys.argv[1])])
